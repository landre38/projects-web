const squares = document.querySelectorAll('.square')
const mole = document.querySelector('.mole')
const timeLeft = document.querySelector('#time-left')
const score = document.querySelector('#score')

let result = 0
let molePosition
let currentTime = 60
let timerId = null
let countDownTimerId = setInterval(countDown, 1000)

function randomSquare() {
    squares.forEach(square => {
      square.classList.remove('mole')  
    })

    let randomSquare = squares[Math.floor(Math.random() * 9)]
    randomSquare.classList.add('mole')
    molePosition = randomSquare.id
}

function moveMole() {
    timerId = setInterval(randomSquare, 1000)
}

function countDown() {
    currentTime--
    timeLeft.textContent = currentTime

    if (currentTime == 0) {
        clearInterval(countDownTimerId)
        clearInterval()
        alert('Game Over! Your final score is' + result)
    }

}

moveMole()

addEventListener("click", (event) => {
    if (event.target.id == molePosition)
    {result = result+1;
    score.textContent = result;
    console.log(molePosition)
    console.log(squares)
    }
})

