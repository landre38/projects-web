let wins = 0; let loses = 0;
const choices = ["pierre", "papier", "ciseaux"];

const gameStatus = document.getElementById("gameStatus");
const gameScore = document.getElementById("gameScore");

const rock = document.getElementById("rock");
const paper = document.getElementById("paper");
const scissors = document.getElementById("scissors");

function runGame(userChoice) {
    const computerChoice = choices[Math.floor(Math.random() * choices.length)];
    
    console.log(`Moi : ${userChoice} | Ordinateur: ${computerChoice}`);

    switch(userChoice + '_'+ computerChoice) {
        case 'papier_ciseaux':
        case 'pierre_papier':
        case 'ciseaux_caillou':
            loses += 1;
            gameStatus.innerHTML = `Moi : ${userChoice} | Ordinateur: ${computerChoice} -> Ordinateur wins`
            break;
        case 'papier_pierre':
        case 'pierre_ciseaux':
        case 'ciseaux_papier':
            wins += 1;
            gameStatus.innerHTML = `Moi : ${userChoice} | Ordinateur: ${computerChoice} -> Moi wins`
            break;
        case 'papier_papier':
        case 'pierre_pierre':
        case 'ciseaux_ciseaux':
            gameStatus.innerHTML = `Moi : ${userChoice} | Ordinateur: ${computerChoice} -> Egalité`
            break;

    }

    gameScore.innerHTML `Moi : ${wins} | Ordinateur: ${loses}`;
}


rock.addEventListener("click", () => console.log("pierre"));
paper.addEventListener("click", () => console.log("papier"));
scissors.addEventListener("click", () => console.log("ciseaux"));